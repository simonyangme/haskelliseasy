---
title: Home
---

# Haskell is easy - a list of recommended Haskell libraries

To make it more true that Haskell is easy, here's a list of curated libraries. In the personality descriptions matching you to a library, be honest with yourself about where you're at and what your needs are.

This isn't listing for listing's sake. I've either used it or someone whose taste and experience I really trust has used it. If you list everything, the list is useless to somebody that can't sniff test the API quickly.

## Getting Haskell installed

[Use Stack!](http://haskellstack.org)

## Basic data structures

- [containers](http://hackage.haskell.org/package/containers)

## Benchmarking

- [criterion](http://hackage.haskell.org/package/criterion) uses [statistics](#statistics) to help prevent you lying to yourself.
  - Covered along with basic data structures in [haskell book](http://haskellbook.com).

## Binary serialization/deserialization

### Stuff that is somewhat opinionated about how your data looks over the wire

- [binary](http://hackage.haskell.org/package/binary)

- [cereal](http://hackage.haskell.org/package/cereal)

### I wanna write my own!

- Use the [bytestring](http://hackage.haskell.org/package/bytestring) library and the builder API.

## CSV

- [cassava](https://hackage.haskell.org/package/cassava)
  * [HowIStart Haskell tutorial using Cassava](http://howistart.org/posts/haskell/1)

- [pipes-csv](http://hackage.haskell.org/package/pipes-csv) streaming, built on Cassava. See the [note on streaming libraries](#note-on-streaming).

## Elasticsearch

- [Bloodhound](http://hackage.haskell.org/package/bloodhound) note you'll have to use Aeson version 0.10 or newer.

## HTTP clients

### I just got here and don't know what I'm doing

- [Wreq](https://hackage.haskell.org/package/wreq)
  - Tutorials and examples:
    - [Wreq tutorial](http://www.serpentine.com/wreq/)
    - [Keeping it easy while querying Github's API](http://bitemyapp.com/posts/2016-02-06-haskell-is-not-trivial-not-unfair.html)

### Know what I'm doing and I'd like more fine-grained control over resources, such as streaming

Please [see the note on streaming](#note-on-streaming) before using any streaming libraries.

- [pipes-http](https://hackage.haskell.org/package/pipes-http)

- [http-conduit](https://hackage.haskell.org/package/http-conduit)


## JSON

- [aeson](http://hackage.haskell.org/package/aeson)

## lenses

- [lens](http://hackage.haskell.org/package/lens) you'll have to cargo cult early on and it's best if you're not a beginner, but there's nothing better if it's something you're going to use instead of framing the source code and mounting it over your fireplace.

## NLP

- [chatter](http://hackage.haskell.org/package/chatter) a collection of simple Natural Language Processing algorithms.

## Parsing

- [attoparsec](http://hackage.haskell.org/package/attoparsec)

- [parsers](http://hackage.haskell.org/package/parsers) with [trifecta](http://hackage.haskell.org/package/trifecta) or [attoparsec](http://hackage.haskell.org/package/attoparsec) as the backend.
  - Covered in [Haskell book](http://haskellbook.com/) in the chapter on parsers.

## Regular expressions

- [regex-tdfa](http://hackage.haskell.org/package/regex-tdfa)

## SQL

### I'm new and just want to throw a query over the wire!

#### PostgreSQL

- [postgresql-simple](hackage.haskell.org/package/postgresql-simple)

#### MySQL

- [mysql-simple](hackage.haskell.org/package/mysql-simple)

#### SQlite

- [sqlite-simple](http://hackage.haskell.org/package/sqlite-simple)

### I'm not totally new to Haskell and willing to invest in something more type-safe!

- [Persistent](hackage.haskell.org/package/persistent) combined with [Esqueleto](hackage.haskell.org/package/esqueleto) as appropriate.
  - [Yesod book chapter on Persistent](http://www.yesodweb.com/book/persistent)
  - For Esqueleto? You're going to trip up a bit early on, especially with the join order. Try to accumulate example code you can work from.

## Statistics

- [statistics](http://hackage.haskell.org/package/statistics)

## Streaming

- [Pipes](http://hackage.haskell.org/package/pipes)

Take the [note on streaming](#note-on-streaming) under advisement.

## Testing

### Unit/spec testing

- [HSpec](http://hspec.github.io/)
  - Covered in [Haskell Book](http://haskellbook.com)'s testing chapter.

### Property testing

- [QuickCheck](http://hackage.haskell.org/package/QuickCheck)
  - [If you find yourself commonly checking typeclass laws](http://hackage.haskell.org/package/checkers)
  - Also covered in [Haskell Book](http://haskellbook.com)'s testing chapter and used throughout the chapters on monoid, semigroup, functor, applicative, and monad.

## Utility libraries and conveniences

- [classy-prelude](http://hackage.haskell.org/package/classy-prelude) can save you some grief, particularly around mixing IO, String, ByteString, and Text. Best used with a [project template](https://github.com/commercialhaskell/stack-templates/blob/master/yesod-hello-world.hsfiles) that disables Prelude by default and gets an import module in place for you.

- [composition-extra](http://hackage.haskell.org/package/composition-extra) mostly for this [bad boy](http://hackage.haskell.org/package/composition-extra-2.0.0/docs/Data-Functor-Syntax.html#v:-60--36--36--62-). Double-fmap! I am easily amused.

## Web Frameworks

### I just need to make a tiny API or I just started

- [Scotty](hackage.haskell.org/package/scotty)
  - [Examples in the Scotty git repository](https://github.com/scotty-web/scotty/tree/master/examples)
  - [Literate URL shortener](http://bitemyapp.com/posts/2014-11-22-literate-url-shortener.html)
  - [Compact URL shortener](http://bitemyapp.com/posts/2014-08-22-url-shortener-in-haskell.html)

### I'm going to make a web application I have to maintain and I'm comfortable with a more opinionated framework

- [Yesod](http://www.yesodweb.com/) also better for APIs if you can handle something more advanced than Scotty.
  - [Yesod book](http://www.yesodweb.com/book)
  - [Carnival](https://github.com/thoughtbot/carnival) open source comments app.
  - [Snowdrift.coop](https://github.com/snowdriftcoop/snowdrift) a non-profit, cooperative platform for funding public goods, specifically Free/Libre/Open (FLO) works.

## XML

- [taggy](https://hackage.haskell.org/package/taggy) and [taggy-lens](https://hackage.haskell.org/package/taggy-lens).

- [xml-conduit](https://hackage.haskell.org/package/xml-conduit) the usual caveats around streaming apply.

## Note on streaming

Pipes is generally easier to use than Conduit and can be quite pleasant. However, if you're new to Haskell you should avoid using streaming libraries unless necessary to control memory usage. Mostly because you'll spin your wheels on which operator to use and the type errors won't help much. YMMV. Please don't email me to brag about how you're a genius-wizard that had zero trouble using streaming libraries unless you're going to link a resource or explain a method that made it easy. And if the link is to the standard tutorial for the library, I will not respect you.
